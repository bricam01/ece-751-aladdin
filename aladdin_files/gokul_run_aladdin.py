# Runs dumped checkpoints

#!/usr/bin/env python
import os
import sys
import shutil
import errno

rundir_val = sys.argv[1]
acc_size = sys.argv[2]
acc_assoc = sys.argv[3]
config = acc_size + '_' + acc_assoc
gem5dir = "/home/sgokul/aladdin_gem5/ece-751-aladdin"
home_dir = "/research/sgokul"
from aladdin_benchmarks import benchmarks
submit_script=""
maindir = "/research/sgokul/spec2006_cluster"
for benchmark,settings in benchmarks.items():
  simpoints = 4  # TODO Just called simpoint    
  for simpoint in xrange(simpoints):
    if simpoint == 0:
	pareto_string = "p"
    elif simpoint == 1:
	pareto_string = "d"
    elif simpoint == 2:
	pareto_string = "pd"
    else :
	pareto_string = "pd2"	

    rundir = os.path.join(home_dir, rundir_val, benchmark, acc_size, acc_assoc, str(pareto_string)) #Here
    #rundir = str(home_dir) + "/" + str(rundir_val) + "/" + str(benchmark) + "/" + str(config) + "/" + str(pareto_string)

    shutil.copytree(str(maindir) + "/aladdin_traces/" + str(benchmark), rundir)
    shutil.copy(str(maindir) + "/aladdin_binaries/" + str(benchmark), rundir)    

    if benchmark == "bb_gemm":
	gva = "0x6c3820"
        ova = "0x605090"
    elif benchmark == "triad":
	gva = "0x6c3820"
	ova = "0x6030a0"
    elif benchmark == "pp_scan":
	gva = "0x6c3820"
	ova = "0x608094"
    else: #red
	gva = "0x6c3820"
	ova = "0x603090"

    gem5cmd = gem5dir + "/build/X86/gem5.opt "
    # simulator python script + config
    #TODO
    gem5cmd += gem5dir + "/configs/example/se.py --cpu-type=timing --caches --l1d_size=64kB --l1i_size=64kB --l2cache --l2_size=2MB --aladdin --aladdin-trace " + pareto_string + ".txt --aladdin-gem5-va " + gva + " --aladdin-orig-va " + ova + " --acc_size " + acc_size + " --acc_assoc " + acc_assoc

    # application binary
    gem5cmd += " -c " + benchmark + " "
    
    #gem5cmd += "--stdout=" + benchmark + ".stdout "
    #gem5cmd += "--stderr=" + benchmark + ".stderr "

    sh_script = """#!/bin/sh
%s"""  % gem5cmd

    sh_file = open(rundir + "/job.sh", "w+")
    sh_file.write(sh_script)
    sh_file.close()

    print sh_script 
    submit_script+="""
executable = /bin/sh 
arguments = %s
initialdir = %s
output = %s
error = %s
log = %s
Rank=TARGET.Mips
universe = vanilla
getenv = true
queue
    """ % (rundir + "/job.sh",
           rundir,
           rundir + "/gem5sim.out",
           rundir + "/gem5sim.err",
           rundir + "/gem5sim.log")


print submit_script 
sub_file = open(config+"condor_submit.scr", "w+") #TODO
#sub_file = open("condor_submit.scr", "w+")
sub_file.write(submit_script)
sub_file.close()
#os.system("condor_submit condor_submit.scr")
