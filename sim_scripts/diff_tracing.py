#!/usr/bin/pyhton 

import os
import sys

import memtrace_pb2

#-- quick script to post process Aladdin trace
#   to the format required for gem5
#

def postproc(tracefile, outfile):

    #-- first sort the operations based on cycle number
    os.system("cat %s  | sort -g > %s.sort -o %s" % (tracefile, tracefile,tracefile+"2"));

    #-- next we want to have the traces in a differential format :
    #   each cycle number indicates how far it is from the other 
    
    #-- also add cycle by cyle values to the trace file

    #--serializing the trace with protocol buffers
    #--  essentially it is just one time effort to
    #--  specify the format and protobuf handles
    #--  all the formatting and file i/o 
    #--  see memtrace.proto for the format of the trace items




    print " Dumping trace to : ", outfile

    memtrace = memtrace_pb2.Memtrace()
    			

    print " Opening file : ", tracefile

    with open(tracefile+"2") as f:
	lineNum=0;
	prevCycle=0;
	for line in f:
		if lineNum!=0:	# first line of aladdin is the name descriptors, not values
			
			memtracei = memtrace.traceitem.add() # add trace item
			
			currentCycle = int(line.split()[0][:-1]) #extract cycle num
			
			memtracei.cycle = currentCycle-prevCycle #find difference from last
			
			prevCycle=currentCycle			#update cycle for next read
			
			memtracei.vaddr = int(line.split()[3])  #insert address
			
			if line.split()[1].find("load") != -1:        #see if it is a load
				memtracei.memtype= memtrace_pb2.MemtraceItem.READ
				print "lol"
				
			else:
				memtracei.memtype= memtrace_pb2.MemtraceItem.WRITE
			print line.split()[1]	
		lineNum=lineNum+1

    f = open(outfile, "wb")
    f.write(memtrace.SerializeToString())
    f.close()

#-- test function
def test_protoc():
    memtrace = memtrace_pb2.Memtrace()

    f = open(sys.argv[2], "rb")
    memtrace.ParseFromString(f.read())
    f.close() 

    for item in memtrace.traceitem:
        if item.memtype == memtrace_pb2.MemtraceItem.WRITE :
            print "Item : Write vaddr = %x, cycle = %d" % (item.vaddr, item.cycle)
        else:
            print "Item : Read vaddr = %x, cycle = %d" % (item.vaddr, item.cycle)



def main():

    postproc(sys.argv[1], sys.argv[2])
    test_protoc()

main()
