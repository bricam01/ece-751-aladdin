#!/usr/bin/perl -w

use warnings;

%cpubases = ( 

"DATA_x-0"     => 0x00000000006c7370 ,
"DATA_x-1"     => 0x00000000006c7370 ,
"DATA_y-0"     => 0x00000000006c7b70 ,
"DATA_y-1"     => 0x00000000006c7b70 ,
"work_x-0"        => 0x00000000006c8370 ,
"work_x-1"        => 0x00000000006c8370 ,
"work_y-0"        => 0x00000000006c8b70 ,
"work_y-1"        => 0x00000000006c8b70 ,
"cos_512"    => 0x00000000006c5570 ,
"cos_64"     => 0x00000000006c4e70 ,
"data_x"     => 0x00000000006c9370 ,
"data_y"     => 0x00000000006c9390 ,
"reversed"   => 0x00000000006c5c70 ,
"sin_512"    => 0x00000000006c4770 ,
"sin_64"     => 0x00000000006c4070 ,
"smem-0"       => 0x00000000006c93b0 ,
"smem-1"       => 0x00000000006c93b0 ,


);

#%cpubases = ( 
#
#"DATA_x-0" =>     0x0000000006c9370 ,
#"DATA_y-0" =>     0x00000000006c9b70 ,
#"DATA_x-1" =>     0x0000000006c9370 ,
#"DATA_y-1" =>     0x00000000006c9b70 ,
#"work_x-0" =>        0x00000000006c8370 ,
#"work_x-1" =>        0x00000000006c8370 ,
#"work_y-0" =>        0x00000000006c8b70 ,
#"work_y-1" =>        0x00000000006c8370 ,
#"cos_512" =>    0x00000000006c6570  ,
#"cos_64" =>     0x00000000006c5e70 ,
#"data_x" =>     0x00000000006ca370 ,
#"data_y" =>     0x00000000006ca390 ,
#"reversed" =>   0x00000000006c6c70   ,
#"sin_512" =>    0x00000000006c5770  ,
#"sin_64" =>     0x00000000006c5070 ,
#"smem-0" =>       0x00000000006ca3b0 ,
#"smem-1" =>       0x00000000006ca3b0 ,
#
#);

%aladdinbases = (

"DATA_x-0"  => 6974624,
"DATA_x-1"  => 6974624,
"DATA_y-0"  => 6976672,
"DATA_y-1"  => 6976672,
"cos_512"  => 6968672,
"cos_64"  => 6965088,
"data_x"  => 6978720,
"data_y"  => 6978752,
"reversed"  => 6970464,
"sin_512"  => 6966880,
"sin_64"  => 6963296,
"smem-0"  => 6978784,
"smem-1"  => 6978784,
"work_x-0"  => 6970528,
"work_x-1"  => 6970528,
"work_y-0"  => 6972576,
"work_y-1"  => 6972576
);

sub getbase_addresses  {

    $tracefile = shift;

    %min = ();
    %max = ();

    # read the elements and find the minimum
    # and maximum
    foreach $key (keys %cpubases) {
        print "============================\n\n";
        print $key, "\n";
        print "----------------------------\n";

        open(ARRAY, "grep $key $tracefile|" );
        $min = 100000000000;
        while(<ARRAY>) {
            #print $_;
            chomp;

            #2, store, data_x, 6978720
            if($_ =~ m/(\d+), (\w+), ([\w-]+), (\d+)/)
            {
                $num  = $1;
                $ldst  = $2;
                $array  = $3;
                $cycle  = $4;
                if($array ne $key) {
                    print "Error:\n";
                    exit;
                }
                #print "$num, $ldst, $array, $cycle\n";

                if($cycle < $min) { $min = $cycle;}
            }
                
        }

        print " -(min) $array  = $min\n";
        print "============================\n\n";
        close(ARRAY);
    }
};

sub replace_baseaddr  {

    $tracefile = shift;

    %min = ();
    %max = ();

    # read the elements and translate them to gem5 version addresses
    open(ARRAY, "< $tracefile" );

    while(<ARRAY>) {
        chomp;

        #2, store, data_x, 6978720
        if($_ =~ m/(\d+), (\w+), ([\w-]+), (\d+)/)
        {
            $num  = $1;
            $ldst  = $2;
            $array  = $3;
            $addr  = $4;

            $found = 0;
            foreach $key (keys %cpubases) {
                $found =1 if ($key eq $array);
            }

            die "Error: not found array : $array\n" if(not $found);

            # translate address
            #printf " translating $array , %x => %x\n", $aladdinbases{$array} ,  $cpubases{$array};
            $addr = $addr - $aladdinbases{$array} + $cpubases{$array};
            print "$num, $ldst, $array, $addr\n";

        }
        else {
            ###print "Ignored: ", $_, "\n";
            print $_, "\n";
        }
            
    }

    #print "============================\n\n";
    close(ARRAY);
}

#getbase_addresses($ARGV[0]);
replace_baseaddr($ARGV[0]);
