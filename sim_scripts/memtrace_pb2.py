# Generated by the protocol buffer compiler.  DO NOT EDIT!

from google.protobuf import descriptor
from google.protobuf import message
from google.protobuf import reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)



DESCRIPTOR = descriptor.FileDescriptor(
  name='memtrace.proto',
  package='aladdin',
  serialized_pb='\n\x0ememtrace.proto\x12\x07\x61laddin\"|\n\x0cMemtraceItem\x12\r\n\x05\x63ycle\x18\x01 \x02(\x05\x12\r\n\x05vaddr\x18\x02 \x02(\x05\x12.\n\x07memtype\x18\x03 \x02(\x0e\x32\x1d.aladdin.MemtraceItem.Memtype\"\x1e\n\x07Memtype\x12\x08\n\x04READ\x10\x00\x12\t\n\x05WRITE\x10\x01\"4\n\x08Memtrace\x12(\n\ttraceitem\x18\x01 \x03(\x0b\x32\x15.aladdin.MemtraceItem')



_MEMTRACEITEM_MEMTYPE = descriptor.EnumDescriptor(
  name='Memtype',
  full_name='aladdin.MemtraceItem.Memtype',
  filename=None,
  file=DESCRIPTOR,
  values=[
    descriptor.EnumValueDescriptor(
      name='READ', index=0, number=0,
      options=None,
      type=None),
    descriptor.EnumValueDescriptor(
      name='WRITE', index=1, number=1,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=121,
  serialized_end=151,
)


_MEMTRACEITEM = descriptor.Descriptor(
  name='MemtraceItem',
  full_name='aladdin.MemtraceItem',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='cycle', full_name='aladdin.MemtraceItem.cycle', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='vaddr', full_name='aladdin.MemtraceItem.vaddr', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    descriptor.FieldDescriptor(
      name='memtype', full_name='aladdin.MemtraceItem.memtype', index=2,
      number=3, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _MEMTRACEITEM_MEMTYPE,
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=27,
  serialized_end=151,
)


_MEMTRACE = descriptor.Descriptor(
  name='Memtrace',
  full_name='aladdin.Memtrace',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    descriptor.FieldDescriptor(
      name='traceitem', full_name='aladdin.Memtrace.traceitem', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=153,
  serialized_end=205,
)

_MEMTRACEITEM.fields_by_name['memtype'].enum_type = _MEMTRACEITEM_MEMTYPE
_MEMTRACEITEM_MEMTYPE.containing_type = _MEMTRACEITEM;
_MEMTRACE.fields_by_name['traceitem'].message_type = _MEMTRACEITEM
DESCRIPTOR.message_types_by_name['MemtraceItem'] = _MEMTRACEITEM
DESCRIPTOR.message_types_by_name['Memtrace'] = _MEMTRACE

class MemtraceItem(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _MEMTRACEITEM
  
  # @@protoc_insertion_point(class_scope:aladdin.MemtraceItem)

class Memtrace(message.Message):
  __metaclass__ = reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _MEMTRACE
  
  # @@protoc_insertion_point(class_scope:aladdin.Memtrace)

# @@protoc_insertion_point(module_scope)
