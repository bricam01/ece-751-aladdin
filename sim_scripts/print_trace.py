#!/usr/bin/python 

import os
import sys

import memtrace_pb2

#-- quick script to print aladdin trace


#-- test function
def test_protoc():
    memtrace = memtrace_pb2.Memtrace()

    f = open(sys.argv[1], "rb")
    memtrace.ParseFromString(f.read())
    f.close() 

    for item in memtrace.traceitem:
        if item.memtype == memtrace_pb2.MemtraceItem.WRITE :
            print "Item : Write vaddr = %x, cycle = %d" % (item.vaddr, item.cycle)
        else:
            print "Item : Read vaddr = %x, cycle = %d" % (item.vaddr, item.cycle)



def main():
    test_protoc()

main()
