
from m5.params import *
from m5.proxy import *
from MemObject import MemObject
from m5.SimObject import SimObject 


class AccTLB(SimObject):
    type = 'AccTLB'
    cxx_header = "mem/aladdin/aladdin_tlb.hh"

    size = Param.Int("Capacity")
    assoc = Param.Unsigned("Associativity")

    prefetch = Param.Bool(False,
        "enable next page prefetching into TLB")

    prefetch_degree = Param.Int(1,
        "pre-fetch degree")

class AladdinAcc(MemObject):
    type = 'AladdinAcc'
    cxx_header = "mem/aladdin/aladdin_acc.hh"
# for testing, activate after these many ticks
    initcycle = Param.Unsigned(1000, "init time")
    accport = MasterPort("Acc port")
    tracefile = Param.String("test", "Trace file with memory trace")
    gva = Param.Int(0, "Gem5 VA Offset")
    ova = Param.Int(0, "Orig VA Offset")
     
    tlb = Param.AccTLB( AccTLB(size = '64', assoc = 64, prefetch=False) , "Accelerator trace based TLB model")
    system = Param.System(Parent.any, "System we belong to")
