/*
 * Copyright (c) 2012, 2015 ARM Limited
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2006 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ali Saidi
 *          Nathan Binkert
 *          Andreas Hansson
 *          Andreas Sandberg
 */

#include "mem/aladdin/aladdin_acc.hh"

#include <utility>

#include "debug/Aladdin.hh"
#include "sim/system.hh"
//#include "arch/x86/vtophys.hh"

//Gokul additions
#include "arch/isa_traits.hh"
#include "base/misc.hh"
#include "cpu/base.hh"
#include "cpu/thread_context.hh"
#include "mem/page_table.hh"
#include "sim/full_system.hh"
#include "sim/process.hh"


#define TRACE_ITEM_CHUNK_SIZE  2048

AccPort::AccPort(AladdinAcc *dev, System *s, AccTLB *tlb, int gva, int ova)
    : MasterPort(dev->name() + ".dma", dev),
      acc(dev), tlb(tlb), sys(s), masterId(1), gva(gva), ova(ova),
      sendEvent(this), pendingCount(0), transmitCount(0), inRetry(false)
{ 
}

void
AccPort::handleResp(PacketPtr pkt, Tick delay)
{
    // should always see a response with a sender state
    assert(pkt->isResponse());

    // get the DMA sender state
    //DmaReqState *state = dynamic_cast<DmaReqState*>(pkt->senderState);
    //assert(state);
    
    // read resp data
    /*- --brian : can't really read the data in the right way.
    int size = pkt->req->getSize();
    uint8_t *data = new uint8_t[size];
    pkt->setData(data);

    int datan = *(reinterpret_cast<int*> (data));
    */

  // data = %d\n" ,
    DPRINTF(Aladdin, "Received response %s for addr: %#x size: %d\n",
            pkt->cmdString(), pkt->getAddr(), pkt->req->getSize());
            //" tot: %d sched %d\n",
            //state->numBytes, state->totBytes,
            //state->completionEvent ?
            //state->completionEvent->scheduled() : 0);

    assert(pendingCount != 0);
    pendingCount--;

    // update the number of bytes received based on the request rather
    // than the packet as the latter could be rounded up to line sizes
    //state->numBytes += pkt->req->getSize();
    //assert(state->totBytes >= state->numBytes);

    // if we have completed all requests scheduled in this cycle
    // then signal the completion and delete the sate
    /*
    if (state->totBytes == state->numBytes) {
        if (state->completionEvent) {
            delay += state->delay;
            acc->schedule(state->completionEvent, curTick() + delay);
        }
        delete state;
    }
    */

    // If we have no more pending transactions schedule the
    // next mem transaction

    if(pendingCount == 0 && transmitList.size()) {
            int next_delay = lookupNextDelay();

            assert(next_delay > 0); // the next bunch of transaction must be atlease 1 cycle apart
            //acc->schedule(sendEvent, acc->clockEdge(Cycles(1)));
            //acc->schedule(state->completionEvent, curTick() + delay);

            // TBD: cleanup: 
            /*
            if(next_delay == 1)
                acc->schedule(sendEvent, curTick());
            else {
                // are we aligned to this clock edge ?
                if(acc->curCycle() == Cycles( curTick() / acc->clockPeriod()))
                    acc->schedule(sendEvent, acc->clockEdge( Cycles(next_delay-1) ));
                else
                    acc->schedule(sendEvent, acc->clockEdge( Cycles(next_delay-2) ));
            }
            */
            //acc->schedule(sendEvent, acc->clockEdge( Cycles(next_delay-1) ));
            if(acc->curCycle() == Cycles( curTick() / acc->clockPeriod())  && next_delay == 1)
                acc->schedule(sendEvent, curTick());
            else
                acc->schedule(sendEvent, acc->clockEdge( Cycles(next_delay-1) ));
    }
    
    // read new transactions if done
    if(pendingCount == 0 && transmitList.size() == 0) {
        acc->loadTrace();
    }

    // delete the request that we created and also the packet
    delete pkt->req;
    delete pkt;

    // we might be drained at this point, if so signal the drain event
    //if (pendingCount == 0)
        //signalDrainDone();
}

bool
AccPort::recvTimingResp(PacketPtr pkt)
{
    // We shouldn't ever get a cacheable block in ownership state
    //assert(pkt->req->isUncacheable() ||
     //      !(pkt->memInhibitAsserted() && !pkt->sharedAsserted()));

    handleResp(pkt);

    return true;
}

AladdinAcc::AladdinAcc(const Params *p)
    :  MemObject(p), accPort(this, p->system, p->tlb, p->gva, p->ova), initcycle(p->initcycle), 
       system(p->system), tracefile(p->tracefile), tlb(p->tlb), _acc_done(false), invokeEvent(this)
{ }

BaseMasterPort &
AladdinAcc::getMasterPort(const std::string &if_name, PortID idx)
{
    if (if_name == "accPort") {
        return accPort;
    }
    return accPort;
}

void
AladdinAcc::init()
{
    if (!accPort.isConnected())
        panic("Aladdin port of %s not connected to anything!", name());

    // register this accelerator with system
    system->accList.push_back(this);

    setupTraceRead();
    //schedule(invokeEvent, Tick(initcycle));
}

void 
AladdinAcc::setupTraceRead()
{
    std::fstream input(tracefile, std::ios::in | std::ios::binary);

   if (!input) {
      std::cerr << tracefile << ": File not found. (aladdin trace)" << std::endl;
      fatal("Aladdin: could not load trace");
    } else if (!memtrace.ParseFromIstream(&input)) {
      fatal("Aladdin: Failed to parse memory trace" );
    }

    // set initial trace index = 0
    traceindex = 0;

    input.close();
    // load first N elements of trace
    // loadTrace();
}

void
AladdinAcc::loadTrace()
{
    int tracesize = memtrace.traceitem_size();

    int count = TRACE_ITEM_CHUNK_SIZE;
    int index =traceindex;
    
    // if we are done exit accelerator
    if(traceindex >= tracesize && !_acc_done)
    {
        std::cout << "[aladdin]  accelerator done at " << curTick() << " tick" << std::endl;
       _acc_done = true;

       // register end tick

       totalCycles = ticksToCycles(curTick() - starttick);

       // wake up CPU
       // TBD : change base cpu id from invoke function
        int cpuid = 0;
        std::cout << "[aladdin] Waking up CPU : " << cpuid <<  std::endl;
        ThreadContext *cpu_tc = system->threadContexts[cpuid];
        if (cpu_tc->status() == ThreadContext::Suspended)
            cpu_tc->activate();
    }

    static uint8_t datatemp[TRACE_ITEM_CHUNK_SIZE*8];

    uint8_t *rdata = &datatemp[0];
    while(index < tracesize && count > 0)
    {
        auto memitem = memtrace.traceitem(index);

        int cycle = memitem.cycle();
        int vaddr = memitem.vaddr();
        int size = (memitem.has_size()) ? memitem.size() : 4; 

        DPRINTF(Aladdin, "Loading trace item: %d: addr =%x, nx_cycle =%d\n" ,
                 index, vaddr, cycle );
        if(memitem.memtype() == aladdin::MemtraceItem::READ) {
            accRead(vaddr, size, 0, rdata, cycle);
        } else {
            accWrite(vaddr, size, 0, rdata, cycle);
        }
        count--;
        index++;
        rdata += size;

        // update ideal cycles
        idealCycles += cycle;
    }

    traceindex = index;
}

void
AladdinAcc::invoke()
{
    //const Addr base = 0x6c2760;
    //int size = 8;
    std::cout<<"I have been awoken" << std::endl;
    std::cout << "[aladdin]  accelerator begins at : " << curTick() << " tick" << std::endl;

    // save start tick
    starttick = curTick();

    loadTrace();
    //uint8_t *rdata = new uint8_t[size];
    
    /*
    accRead(base, 4, 0, rdata, 0);
    accRead(base + 0x8, 4, 0, rdata, 1);
    accRead(base + 0x10, 4, 0, rdata, 0);
    accRead(base + 0x18, 4, 0, rdata, 5);
    accWrite(base + 0x20, 4, 0, rdata, 0);
    accWrite(base + 0x28, 4, 0, rdata, 0);
    accRead(base + 0x8, 4, 0, rdata, 1);
    accRead(base + 0x18, 4, 0, rdata, 5);
    */

}

/*
DrainState
AccPort::drain()
{
    if (pendingCount == 0) {
        return DrainState::Drained;
    } else {
        DPRINTF(Drain, "AccPort not drained\n");
        return DrainState::Draining;
    }
}
*/

void
AccPort::recvReqRetry()
{
    assert(transmitList.size());
    trySendTimingReq();
}

RequestPtr
AccPort::memAction(Packet::Command cmd, Addr vaddr, int size, Event *event,
                   uint8_t *data, int delay, Request::Flags flag)
{
    // save virtual address in sender state
    DmaReqState *reqState = new DmaReqState(vaddr);

    // (functionality added for Table Walker statistics)
    // We're only interested in this when there will only be one request.
    // For simplicity, we return the last request, which would also be
    // the only request in that case.
    RequestPtr req = NULL;

    // translate the vaddress
    Addr paddr = translateVaddr(vaddr);

    DPRINTF(Aladdin, "Recording memory op for addr: va %#x pa %#x ,size: %d sched: %d\n", vaddr, paddr, size, event ? event->scheduled() : -1);
    //for (ChunkGenerator gen(addr, size, sys->cacheLineSize());
    //     !gen.done(); gen.next()) {
    req = new Request(paddr, size, flag, masterId);
    //req->taskId(ContextSwitchTaskId::Aladdin);
    PacketPtr pkt = new Packet(req, cmd);

    // Increment the data pointer on a write
    if (data)
        pkt->dataStatic(data);

    pkt->senderState = reqState;

    DPRINTF(Aladdin, "--Queuing DMA for paddr: %#x size: %d, delay:%d\n", paddr,size, delay);
    queueDma(pkt, delay);
    //}

    // in zero time also initiate the sending of the packets we have
    // just created, for atomic this involves actually completing all
    // the requests
    sendDma();

    return req;
}

/*-- In an accelerator use case this function is just adding a memory transaction
 *   to an internal enqueue list, it does NOT send packets out to the memory system-*/
void
AccPort::queueDma(PacketPtr pkt, int delay)
{
    transmitList.push_back( std::make_pair(pkt, delay) );

    transmitCount++;
}

void
AccPort::trySendTimingReq()
{
    // send the first packet on the transmit list and schedule the
    // following send if it is successful
    PacketPtr pkt = transmitList.front().first;
    int next_delay = 0;     // first one is scheduled by default

    //  send multiple packets which have a zero inter delay here
    
    while(next_delay == 0) {

        DPRINTF(Aladdin, "Trying to send %s addr %#x\n", pkt->cmdString(),
                pkt->getAddr());
    
        // NEW* perform a TLB access
        // uses virtual address in sender state
        DmaReqState *state = dynamic_cast<DmaReqState*>(pkt->senderState);
        tlb->access(state->vaddr, pkt->getAddr());

        // if we get a retry  postpone sending packets till a recvretry
        inRetry = !sendTimingReq(pkt);
    
        if (!inRetry) {
            transmitList.pop_front();
            DPRINTF(Aladdin, "-- Done\n");
    
            // decrement transaction count
            transmitCount--;
    
            // remember that we have another packet pending, this will only be
            // decremented once a response comes back
            pendingCount++;
    
            // if there is more to do, then do so
            if (!transmitList.empty())
            {
                // now look up the next packet
                pkt = transmitList.front().first;
                next_delay = transmitList.front().second;;
            /* This part has been moved to after receiving response
                // this should ultimately wait for as many cycles as the
                // acc needs to send the packet, but currently the port
                // does not have any known width so simply wait a single
                // cycle
                acc->schedule(sendEvent, acc->clockEdge(Cycles(1)));
                */
            }
            else
                break;
        } else {
            DPRINTF(Aladdin, "-- Failed, waiting for retry\n");
            break;
        }
    }

    DPRINTF(Aladdin, "TransmitList: %d, inRetry: %d\n",
            transmitList.size(), inRetry);
}

/*-- actually enqueue requests to memory here --*/
void
AccPort::sendDma()
{
    // some kind of selcetion between access methods
    // more work is going to have to be done to make
    // switching actually work
    assert(transmitList.size());

    // if we are either waiting for a retry or are still waiting
    // after sending the last packet, then do not proceed
    // do not send if we have the next delay as non zero and we have pending requests
    if (inRetry || sendEvent.scheduled() || (lookupNextDelay()!= 0  && pendingCount) ) {
        DPRINTF(Aladdin, "Can't send immediately, waiting to send\n");
        return;
    }

    trySendTimingReq();
}

/*-- handle address Translation : uses systems address translation for now --*/
Addr AccPort::translateVaddr(Addr vaddr) 
{
    Addr paddr;
    const int cpuid = 0;
    ThreadContext *tc = sys->getThreadContext(cpuid);
    //TODO - Gokul's hack for bbgemm
    Addr vaddr_pseudo;
    //vaddr_pseudo = vaddr + (0x6c3820 - 0x2484010); //BB_GEMM
    //vaddr_pseudo = vaddr + (0x6c83a0 - 0x6a5560); //FFT
    //vaddr_pseudo = vaddr + (0x6c3820 - 0x605090); //BB_GEMM_re

    vaddr_pseudo = vaddr + ((Addr)gva - (Addr)ova);

    tc->getProcessPtr()->pTable->translate(vaddr_pseudo, paddr);
    DPRINTF(Aladdin,"--result of se translation va: %x, va_p: %x -> pa: %x\n", vaddr,vaddr_pseudo, paddr);
    //paddr = vaddr;
    return paddr;
}

void
AladdinAcc::regStats()
{
    totalCycles.name(name() + ".total_cycles")
               .desc("total number of cycles it took to execute trace: including memory stalls");

    idealCycles.name(name() + ".ideal_cycles")
               .desc("ideal number of cycles to execute trace: no memory stalls");

}

AladdinAcc*
AladdinAccParams::create()
{
    return new AladdinAcc(this);
}
