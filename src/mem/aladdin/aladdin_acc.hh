/*
 * Copyright (c) 2012-2013, 2015 ARM Limited
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2004-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ali Saidi
 *          Nathan Binkert
 *          Andreas Sandberg
 */

#ifndef __AlADDIN_ACC_HH__
#define __AlADDIN_ACC_HH__

#include <deque>
#include <memory>
#include <iostream>
#include <fstream>

//class PageTable;
//#include "sim/process.hh"
#include "arch/x86/utility.hh"
#include "base/circlebuf.hh"
#include "base/statistics.hh"
#include "mem/mem_object.hh"
#include "params/AladdinAcc.hh"
#include "debug/Aladdin.hh" // my debug flag
//#include "sim/drain.hh"
#include "cpu/thread_context.hh"
#include "sim/system.hh"

// TLB
#include "mem/aladdin/aladdin_tlb.hh"

// add protobuf header
#include "mem/aladdin/memtrace.pb.h"

//Gokul additions
#include "base/types.hh"
#include "cpu/static_inst.hh"
#include "sim/stats.hh"
class ThreadContext;



class AladdinAcc;

class AccPort : public MasterPort
{
  private:

    /**
     * Take the first packet of the transmit list and attempt to send
     * it as a timing request. If it is successful, schedule the
     * sending of the next packet, otherwise remember that we are
     * waiting for a retry.
     */
    void trySendTimingReq();

    /**
     * For timing, attempt to send the first item on the transmit
     * list, and if it is successful and there are more packets
     * waiting, then schedule the sending of the next packet. For
     * atomic, simply send and process everything on the transmit
     * list.
     */
    void sendDma();

    /**
     * Handle a response packet by updating the corresponding DMA
     * request state to reflect the bytes received, and also update
     * the pending request counter. If the DMA request that this
     * packet is part of is complete, then signal the completion event
     * if present, potentially with a delay added to it.
     *
     * @param pkt Response packet to handler
     * @param delay Additional delay for scheduling the completion event
     */
    void handleResp(PacketPtr pkt, Tick delay = 0);

    /*-- hack: adding the vaddr as sender state so we can track it with TLB --*/
    struct DmaReqState : public Packet::SenderState
    {

        /** virtual address **/
        const Addr vaddr;

        DmaReqState(Addr va)
            : vaddr(va)
        {}
    };

  public:
    /** The device that owns this port. */
    AladdinAcc *const acc;

    /** The TLB model for cache -**/
    AccTLB *const tlb;

    /** The system that device/port are in. This is used to select which mode
     * we are currently operating in. */
    System *const sys;

    /** Id for all requests */
    const MasterID masterId;

    int gva;
    int ova;

  protected:
    /** maintains a list of all active requests --*/
    std::deque< std::pair<PacketPtr,int> > transmitList;

    /** Event used to schedule a future sending from the transmit list. */
    EventWrapper<AccPort, &AccPort::sendDma> sendEvent;

    /** Number of outstanding packets the dma port has. */
    uint32_t pendingCount;

    /** Number of memory ops in our to-do list (transmitList). */
    uint32_t transmitCount;

    /** If the port is currently waiting for a retry before it can
     * send whatever it is that it's sending. */
    bool inRetry;

    int lookupNextDelay() { return transmitList.front().second; }
  protected:

    bool recvTimingResp(PacketPtr pkt) override;
    void recvReqRetry() override;

    void queueDma(PacketPtr pkt, int delay);

    
    /*-- Use system DTB to translate virtual address --*/
    Addr translateVaddr(Addr vaddr);

  public:

    AccPort(AladdinAcc *dev, System *s, AccTLB *tlb, int gva, int ova);

    RequestPtr memAction(Packet::Command cmd, Addr addr, int size, Event *event,
                         uint8_t *data, int delay, Request::Flags flag = 0);

    bool memPending() const { return pendingCount > 0; }

    //DrainState drain() override;
};

class AladdinAcc : public MemObject
{
   protected:
    AccPort accPort;
    int initcycle;

    /** System we are currently operating in. */
    System *system;
    std::string tracefile;

    /** The TLB model for cache -**/
    AccTLB *const tlb;

    // trace buffer
    aladdin::Memtrace memtrace;
    int traceindex;
    bool _acc_done;
    Tick starttick;

  public:
    typedef AladdinAccParams Params;
    AladdinAcc(const Params *p);
    virtual ~AladdinAcc() { }

    /*-- delay is relative between two request --*/
    void accWrite(Addr addr, int size, Event *event, uint8_t *data,
                  int delay = 0)
    {
        accPort.memAction(MemCmd::WriteReq, addr, size, event, data, delay);
    }

    /*-- delay is relative between two request --*/
    void accRead(Addr addr, int size, Event *event, uint8_t *data,
                 int delay = 0)
    {
        accPort.memAction(MemCmd::ReadReq, addr, size, event, data, delay);
    }

    bool memPending() const { return accPort.memPending(); }

    BaseMasterPort &getMasterPort(const std::string &if_name,
                                  PortID idx = InvalidPortID) override;

    /*--- load memtransactions into master port --*/
    void setupTraceRead();

    /*--- load memtransactions into master port --*/
    //  return true if full trace is done
    void loadTrace();

    /*--- invokes the accelerator : run the memory trace --*/
    void invoke();

    EventWrapper<AladdinAcc, &AladdinAcc::invoke> invokeEvent;

    void init() override;

    //unsigned int cacheBlockSize() const { return sys->cacheLineSize(); }

    // Stats

    void regStats();

    /** The number of cycles it took to execute trace */
    Stats::Scalar totalCycles;

    /** Ideal execuation cycles without memory stalls */
    Stats::Scalar idealCycles;

};

#endif
