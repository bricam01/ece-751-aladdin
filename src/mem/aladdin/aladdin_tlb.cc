/*
 * Copyright (c) 2012, 2015 ARM Limited
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2006 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "mem/aladdin/aladdin_tlb.hh"

#include <utility>

//#include "arch/x86/vtophys.hh"


AccTLB::AccTLB(const Params *p)
    :  SimObject(p), pagesize(4096), pagebits(12), size(p->size), assoc(p->assoc), 
       num_sets(p->size / p->assoc), prefetch(p->prefetch), prefetch_degree(p->prefetch_degree)
{ 
    /*-- create space for TLB entries -*/
    sets.resize(num_sets);
    for(int i = 0; i < num_sets; i++) {
        for(int j=0; j<assoc; j++) {
            AccTLBEntry* entry = new AccTLBEntry;
            entry->reset();
            sets[i].push_back(entry);
        }
    }
}

AccTLB::~AccTLB() {};// delete  sets[0][0];};


AccTLB::AccTLBEntry*
AccTLB::lookup(Addr vaddr)
{
    assert(vaddr == getPage(vaddr));    // page aligned??

    int index = getSet(vaddr);
    assert(index < num_sets);
    auto set = sets[index];

    DPRINTF(AladdinTLB, " Looking up va: %x : set = %d\n", vaddr, index);

    /*-- search the entries in that set --*/
    for(int j=0; j<assoc; j++) {
        AccTLBEntry* entry = set[j];
        if(entry->valid && vaddr == entry->vaddr)
            return entry;
    }

    // else miss
return 0;
}

AccTLB::AccTLBEntry*
AccTLB::find_replacement(Addr vaddr)
{
    /*-- our replacement scheme is - first invalid entries , then LRU*/
    int index = getSet(vaddr);
    assert(index < num_sets);
    auto set = sets[index];

    DPRINTF(AladdinTLB, " Finding replacement for va: %x : set = %d\n", vaddr, index);

    Tick min = MaxTick;
    AccTLBEntry* replace = 0;

    /*-- search the entries in that set --*/
    for(int j=0; j<assoc; j++) {
        AccTLBEntry* entry = set[j];
        if(!entry->valid)
            return entry;
        if(entry->timestamp < min) {
            min = entry->timestamp;
            replace = entry;
        }
    }

    DPRINTF(AladdinTLB, " Evicting va= %x : set = %d ts=%d\n", replace->vaddr, index, replace->timestamp);

    return replace;
}
    
void
AccTLB::install_entry(Addr vaddr, Addr paddr, bool isprefetch = false )
{
    /*-- if already present don't bother -*/
    if(lookup(vaddr))
        return;

    DPRINTF(AladdinTLB, "Adding TLB entry va= %x -> pa = %x, prefetch=%d\n", vaddr, paddr, isprefetch);

    // find a replacement 
    AccTLBEntry* replace = find_replacement(vaddr);
    assert(replace);

    // TBD add logic for unused pre-fetches

    replace->reset();  

    // fill in entries
    replace->valid = true;

    replace->vaddr = vaddr;
    replace->paddr = paddr;
    replace->prefetch = isprefetch;
    replace->timestamp = curTick();

    if(!isprefetch)
        replace->used = true;
}

void
AccTLB::access(Addr vaddr, Addr paddr)
{
    // get the page address
    Addr vpage = getPage(vaddr);
    Addr ppage = getPage(paddr);

    /*- lookup TLB entry -*/
    AccTLBEntry* entry = lookup(vpage);
    tlbAccesses++;

    // handle TLB hits
    if(entry) {
        DPRINTF(AladdinTLB, "Access for va: %x (%x) -> pa = %x, Hit in TLB\n", vaddr, vpage, paddr);
        tlbHits++;

        /*-- update LRU --*/
        entry->timestamp = curTick();
        entry->used = true;

        if(entry->prefetch)
            entry->paddr = ppage;

        return;
    }

    // else handle TLB miss
    DPRINTF(AladdinTLB, "Access for va: %x (%x) -> pa = %x, Miss in TLB\n", vaddr, vpage, paddr);
    tlbMisses++;

    /*-- add TLB entry --*/
    install_entry(vpage, ppage);

    // handle prefetches
    if(prefetch) {
        Addr vpage2 = vpage + pagesize;
        for(int i=0; i < prefetch_degree; i++, vpage2 += pagesize)
        {
            // prefetch the next page into TLB
            DPRINTF(AladdinTLB, "Pre-fetching for vpage: (%x) pd degree %d \n", vpage2, prefetch_degree);
            install_entry(vpage2, 0, true);
        }
    }

}

void
AccTLB::regStats()
{
    tlbAccesses.name(name() + ".accesses")
               .desc("total number of TLB accesses ");

    tlbHits.name(name() + ".hits")
               .desc("total number of TLB hits");

    tlbMisses.name(name() + ".misses")
               .desc("total number of TLB misses");

    tlbPrefetches.name(name() + ".prefetches")
               .desc("total number of TLB prefetch accesse");
}

AccTLB*
AccTLBParams::create()
{
    return new AccTLB(this);
}
