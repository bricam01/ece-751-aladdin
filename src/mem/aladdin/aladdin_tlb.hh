/*
 * Copyright (c) 2012-2013, 2015 UW Madison
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2004-2005 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 */

#ifndef __AlADDIN_ACC_TLB_MODEL_HH__
#define __AlADDIN_ACC_TLB_MODEL_HH__

#include <deque>
#include <memory>
#include <iostream>
#include <fstream>

#include "base/types.hh"
#include "sim/sim_object.hh"
#include "cpu/static_inst.hh"
#include "base/statistics.hh"
#include "sim/stats.hh"
#include "mem/mem_object.hh"
#include "params/AccTLB.hh"
#include "debug/AladdinTLB.hh" // my debug flag

/*-- TLB model:  a trace based TLB model for assessing the sizing requirements
 *    of accelerator TLBs and also project the possible overheads  --*/

class AccTLB  : public SimObject
{

  private:

    struct  AccTLBEntry {

      Addr vaddr;
      Addr paddr;
      
      bool valid;  
      bool used;    // use this to check for un used TLB entries - usually from pre-fetches
      bool prefetch;
      Tick timestamp;

      void reset() 
      {
          valid = false;
          used = false;
      }

    };

    const int pagesize;
    const int pagebits;

    /** TLB  parameters **/
    const int size;

    /** associativity : can set to 1 to model fully assoc **/
    const int assoc;
    const int num_sets;

    /** should we prefetch the next page ? **/
    const bool prefetch; 
    const bool prefetch_degree;


    /** Some data structures modelling the TLB storage 
     */
    typedef std::vector<AccTLBEntry*> AccTLBSet;
    std::vector<AccTLBSet> sets; 
    
    /*- helper functions for TLB -*/
    inline Addr getPage(Addr va) {
        return ( va  & ~(pagesize-1) );
    }

    inline Addr getSet(Addr va) {
        return ( (va >> pagebits) & (num_sets-1) );
    }
    /*-- lookup TLB Entry, null if miss--*/
    AccTLBEntry*  lookup(Addr vaddr);

    /*-- find replacement candidate using LRU -*/
    AccTLBEntry*  find_replacement(Addr vaddr);

    /*-- add a TLB entry into the structure -*/
    void  install_entry(Addr vaddr, Addr paddr, bool isprefetch );

    /*-- install a new mapping : simulate a pre-fetch  -*/
    //void add_prefetch(Addr vaddr);
    
  public:

    typedef AccTLBParams Params;
    AccTLB(const Params *p);
    virtual ~AccTLB();

    /*-- simulate a TLB access */
    void access(Addr vaddr, Addr paddr);

    void regStats();
    /** The number of accesses -*/
    Stats::Scalar tlbAccesses;

    /** Total number of misses: note this only per cycle -*/
    Stats::Scalar tlbHits;
    Stats::Scalar tlbMisses;

    /**- for prefetches */
    Stats::Scalar tlbPrefetches;
};

#endif
