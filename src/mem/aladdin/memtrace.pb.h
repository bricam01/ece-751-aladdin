// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: memtrace.proto

#ifndef PROTOBUF_memtrace_2eproto__INCLUDED
#define PROTOBUF_memtrace_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2004000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2004001 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_message_reflection.h>
// @@protoc_insertion_point(includes)

namespace aladdin {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_memtrace_2eproto();
void protobuf_AssignDesc_memtrace_2eproto();
void protobuf_ShutdownFile_memtrace_2eproto();

class MemtraceItem;
class Memtrace;

enum MemtraceItem_Memtype {
  MemtraceItem_Memtype_READ = 0,
  MemtraceItem_Memtype_WRITE = 1
};
bool MemtraceItem_Memtype_IsValid(int value);
const MemtraceItem_Memtype MemtraceItem_Memtype_Memtype_MIN = MemtraceItem_Memtype_READ;
const MemtraceItem_Memtype MemtraceItem_Memtype_Memtype_MAX = MemtraceItem_Memtype_WRITE;
const int MemtraceItem_Memtype_Memtype_ARRAYSIZE = MemtraceItem_Memtype_Memtype_MAX + 1;

const ::google::protobuf::EnumDescriptor* MemtraceItem_Memtype_descriptor();
inline const ::std::string& MemtraceItem_Memtype_Name(MemtraceItem_Memtype value) {
  return ::google::protobuf::internal::NameOfEnum(
    MemtraceItem_Memtype_descriptor(), value);
}
inline bool MemtraceItem_Memtype_Parse(
    const ::std::string& name, MemtraceItem_Memtype* value) {
  return ::google::protobuf::internal::ParseNamedEnum<MemtraceItem_Memtype>(
    MemtraceItem_Memtype_descriptor(), name, value);
}
// ===================================================================

class MemtraceItem : public ::google::protobuf::Message {
 public:
  MemtraceItem();
  virtual ~MemtraceItem();
  
  MemtraceItem(const MemtraceItem& from);
  
  inline MemtraceItem& operator=(const MemtraceItem& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const MemtraceItem& default_instance();
  
  void Swap(MemtraceItem* other);
  
  // implements Message ----------------------------------------------
  
  MemtraceItem* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const MemtraceItem& from);
  void MergeFrom(const MemtraceItem& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  typedef MemtraceItem_Memtype Memtype;
  static const Memtype READ = MemtraceItem_Memtype_READ;
  static const Memtype WRITE = MemtraceItem_Memtype_WRITE;
  static inline bool Memtype_IsValid(int value) {
    return MemtraceItem_Memtype_IsValid(value);
  }
  static const Memtype Memtype_MIN =
    MemtraceItem_Memtype_Memtype_MIN;
  static const Memtype Memtype_MAX =
    MemtraceItem_Memtype_Memtype_MAX;
  static const int Memtype_ARRAYSIZE =
    MemtraceItem_Memtype_Memtype_ARRAYSIZE;
  static inline const ::google::protobuf::EnumDescriptor*
  Memtype_descriptor() {
    return MemtraceItem_Memtype_descriptor();
  }
  static inline const ::std::string& Memtype_Name(Memtype value) {
    return MemtraceItem_Memtype_Name(value);
  }
  static inline bool Memtype_Parse(const ::std::string& name,
      Memtype* value) {
    return MemtraceItem_Memtype_Parse(name, value);
  }
  
  // accessors -------------------------------------------------------
  
  // required int32 cycle = 1;
  inline bool has_cycle() const;
  inline void clear_cycle();
  static const int kCycleFieldNumber = 1;
  inline ::google::protobuf::int32 cycle() const;
  inline void set_cycle(::google::protobuf::int32 value);
  
  // required int32 vaddr = 2;
  inline bool has_vaddr() const;
  inline void clear_vaddr();
  static const int kVaddrFieldNumber = 2;
  inline ::google::protobuf::int32 vaddr() const;
  inline void set_vaddr(::google::protobuf::int32 value);
  
  // required .aladdin.MemtraceItem.Memtype memtype = 3;
  inline bool has_memtype() const;
  inline void clear_memtype();
  static const int kMemtypeFieldNumber = 3;
  inline ::aladdin::MemtraceItem_Memtype memtype() const;
  inline void set_memtype(::aladdin::MemtraceItem_Memtype value);
  
  // optional int32 size = 4 [default = 4];
  inline bool has_size() const;
  inline void clear_size();
  static const int kSizeFieldNumber = 4;
  inline ::google::protobuf::int32 size() const;
  inline void set_size(::google::protobuf::int32 value);
  
  // @@protoc_insertion_point(class_scope:aladdin.MemtraceItem)
 private:
  inline void set_has_cycle();
  inline void clear_has_cycle();
  inline void set_has_vaddr();
  inline void clear_has_vaddr();
  inline void set_has_memtype();
  inline void clear_has_memtype();
  inline void set_has_size();
  inline void clear_has_size();
  
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  
  ::google::protobuf::int32 cycle_;
  ::google::protobuf::int32 vaddr_;
  int memtype_;
  ::google::protobuf::int32 size_;
  
  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(4 + 31) / 32];
  
  friend void  protobuf_AddDesc_memtrace_2eproto();
  friend void protobuf_AssignDesc_memtrace_2eproto();
  friend void protobuf_ShutdownFile_memtrace_2eproto();
  
  void InitAsDefaultInstance();
  static MemtraceItem* default_instance_;
};
// -------------------------------------------------------------------

class Memtrace : public ::google::protobuf::Message {
 public:
  Memtrace();
  virtual ~Memtrace();
  
  Memtrace(const Memtrace& from);
  
  inline Memtrace& operator=(const Memtrace& from) {
    CopyFrom(from);
    return *this;
  }
  
  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }
  
  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }
  
  static const ::google::protobuf::Descriptor* descriptor();
  static const Memtrace& default_instance();
  
  void Swap(Memtrace* other);
  
  // implements Message ----------------------------------------------
  
  Memtrace* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Memtrace& from);
  void MergeFrom(const Memtrace& from);
  void Clear();
  bool IsInitialized() const;
  
  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  
  ::google::protobuf::Metadata GetMetadata() const;
  
  // nested types ----------------------------------------------------
  
  // accessors -------------------------------------------------------
  
  // repeated .aladdin.MemtraceItem traceitem = 1;
  inline int traceitem_size() const;
  inline void clear_traceitem();
  static const int kTraceitemFieldNumber = 1;
  inline const ::aladdin::MemtraceItem& traceitem(int index) const;
  inline ::aladdin::MemtraceItem* mutable_traceitem(int index);
  inline ::aladdin::MemtraceItem* add_traceitem();
  inline const ::google::protobuf::RepeatedPtrField< ::aladdin::MemtraceItem >&
      traceitem() const;
  inline ::google::protobuf::RepeatedPtrField< ::aladdin::MemtraceItem >*
      mutable_traceitem();
  
  // @@protoc_insertion_point(class_scope:aladdin.Memtrace)
 private:
  
  ::google::protobuf::UnknownFieldSet _unknown_fields_;
  
  ::google::protobuf::RepeatedPtrField< ::aladdin::MemtraceItem > traceitem_;
  
  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(1 + 31) / 32];
  
  friend void  protobuf_AddDesc_memtrace_2eproto();
  friend void protobuf_AssignDesc_memtrace_2eproto();
  friend void protobuf_ShutdownFile_memtrace_2eproto();
  
  void InitAsDefaultInstance();
  static Memtrace* default_instance_;
};
// ===================================================================


// ===================================================================

// MemtraceItem

// required int32 cycle = 1;
inline bool MemtraceItem::has_cycle() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void MemtraceItem::set_has_cycle() {
  _has_bits_[0] |= 0x00000001u;
}
inline void MemtraceItem::clear_has_cycle() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void MemtraceItem::clear_cycle() {
  cycle_ = 0;
  clear_has_cycle();
}
inline ::google::protobuf::int32 MemtraceItem::cycle() const {
  return cycle_;
}
inline void MemtraceItem::set_cycle(::google::protobuf::int32 value) {
  set_has_cycle();
  cycle_ = value;
}

// required int32 vaddr = 2;
inline bool MemtraceItem::has_vaddr() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void MemtraceItem::set_has_vaddr() {
  _has_bits_[0] |= 0x00000002u;
}
inline void MemtraceItem::clear_has_vaddr() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void MemtraceItem::clear_vaddr() {
  vaddr_ = 0;
  clear_has_vaddr();
}
inline ::google::protobuf::int32 MemtraceItem::vaddr() const {
  return vaddr_;
}
inline void MemtraceItem::set_vaddr(::google::protobuf::int32 value) {
  set_has_vaddr();
  vaddr_ = value;
}

// required .aladdin.MemtraceItem.Memtype memtype = 3;
inline bool MemtraceItem::has_memtype() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void MemtraceItem::set_has_memtype() {
  _has_bits_[0] |= 0x00000004u;
}
inline void MemtraceItem::clear_has_memtype() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void MemtraceItem::clear_memtype() {
  memtype_ = 0;
  clear_has_memtype();
}
inline ::aladdin::MemtraceItem_Memtype MemtraceItem::memtype() const {
  return static_cast< ::aladdin::MemtraceItem_Memtype >(memtype_);
}
inline void MemtraceItem::set_memtype(::aladdin::MemtraceItem_Memtype value) {
  GOOGLE_DCHECK(::aladdin::MemtraceItem_Memtype_IsValid(value));
  set_has_memtype();
  memtype_ = value;
}

// optional int32 size = 4 [default = 4];
inline bool MemtraceItem::has_size() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void MemtraceItem::set_has_size() {
  _has_bits_[0] |= 0x00000008u;
}
inline void MemtraceItem::clear_has_size() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void MemtraceItem::clear_size() {
  size_ = 4;
  clear_has_size();
}
inline ::google::protobuf::int32 MemtraceItem::size() const {
  return size_;
}
inline void MemtraceItem::set_size(::google::protobuf::int32 value) {
  set_has_size();
  size_ = value;
}

// -------------------------------------------------------------------

// Memtrace

// repeated .aladdin.MemtraceItem traceitem = 1;
inline int Memtrace::traceitem_size() const {
  return traceitem_.size();
}
inline void Memtrace::clear_traceitem() {
  traceitem_.Clear();
}
inline const ::aladdin::MemtraceItem& Memtrace::traceitem(int index) const {
  return traceitem_.Get(index);
}
inline ::aladdin::MemtraceItem* Memtrace::mutable_traceitem(int index) {
  return traceitem_.Mutable(index);
}
inline ::aladdin::MemtraceItem* Memtrace::add_traceitem() {
  return traceitem_.Add();
}
inline const ::google::protobuf::RepeatedPtrField< ::aladdin::MemtraceItem >&
Memtrace::traceitem() const {
  return traceitem_;
}
inline ::google::protobuf::RepeatedPtrField< ::aladdin::MemtraceItem >*
Memtrace::mutable_traceitem() {
  return &traceitem_;
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace aladdin

#ifndef SWIG
namespace google {
namespace protobuf {

template <>
inline const EnumDescriptor* GetEnumDescriptor< ::aladdin::MemtraceItem_Memtype>() {
  return ::aladdin::MemtraceItem_Memtype_descriptor();
}

}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_memtrace_2eproto__INCLUDED
