#include <stdio.h>
#define LEN 2028

int a, b;

static int shared_array[LEN];

int main()
{
    printf("Initializing values\n");

    // init value
    int i = 0;
    for(i =0; i < LEN; i++)
        shared_array[i] = i;

    a = i;
    b = 0;

    printf(" Virtual address of shared_array = %p, a = %p, b = %p \n", &shared_array[0], &a, &b);

    printf("Invoking accelerator \n");


    m5_call_acc();
    /* --  accelerate this
    int sum = 0;
    for(int i =0; i < LEN; i++)
        sum += shared_array[i];
    */

    printf("Done program \n");

    return 0;
}
